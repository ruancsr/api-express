const express = require('express')
const app = express()
 
app.get('/', function (req, res) {
  res.send('Hello World')
})

const port = process.env.SERVER_PORT;

app.listen(port)